import os

# Masukkan path folder
path = "."
folder_name = os.path.basename(os.getcwd()) 
total_activity = folder_name.split("-")[-1].split("action")[0]

# Default error selenium bot
errors = [
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-login-page",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-site-home",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-site-course",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION quiz",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-start-quiz-page",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION memulai-quiz",
    "main_worker ERROR: Message: element click intercepted"
]

def failure_rate(total_fail, total_files):
    return 100 * float(total_fail)/float(total_files)

def total_fail(files):
    total_fail = 0
    for filename in files:
        if check_error(filename):
            total_fail += 1
    return total_fail

def check_error(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if any(ele in line for ele in errors):
                return True
    return False


files = [f for f in os.listdir(path) if "report-log.txt" in f]
total_files = len(files)
failnum = total_fail(files)

f = open("InfluxDB_Data.csv","r")
lines = f.readlines()
total_grafana_action = len(lines) - 1
total_actual_action = (100-failnum) * int(total_activity)
print("{},{},{},".format(folder_name,round(failnum/100,3),round(1 - (float(total_grafana_action)/float(total_actual_action)),3)))