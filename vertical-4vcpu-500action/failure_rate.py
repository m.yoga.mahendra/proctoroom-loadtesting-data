import os, sys, csv

# init
csv_path = "D:\\tempdel\\Tugas Akhir\\output\\failure_rate_final.csv" #absolute path
folder_path = "."
folder_name = os.path.basename(os.getcwd()) 
total_activity = folder_name.split("-")[-1].split("action")[0]

# Default error attempt
errors = [
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-login-page",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-site-home",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-site-course",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION quiz",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION mengakses-start-quiz-page",
    "ERROR: TIMEOUT EXCEPTION!@#KODE 0!@#LOCATION memulai-quiz",
    "main_worker ERROR: Message: element click intercepted"
]

def failure_rate_attempt(total_failure, total_files):
    return 100 * float(total_failure)/float(total_files)

def failure_rate_influx(total_influx_action, total_actual_action):
    return 100 - (100 * float(total_influx_action)/float(total_actual_action))

def total_fail(files):
    total_fail = 0
    for filename in files:
        if check_error(filename):
            total_fail += 1
    return total_fail

def check_error(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if any(ele in line for ele in errors):
                return True
    return False

def add_to_csv(filename, arr):
    with open(filename, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(arr)

# Failure Rate Attempt
files = [f for f in os.listdir(folder_path) if "report-log.txt" in f]
total_files = len(files)
total_failure = total_fail(files)
failure_rate_attempts = failure_rate_attempt(total_failure, total_files)

# Failure Rate InfluxDB
f = open("InfluxDB_Data.csv","r")
lines = f.readlines()
total_influx_action = len(lines)-1
total_actual_action = (100-total_failure)*total_activity
failure_rate_influxdb = failure_rate_influx(total_influx_action, total_actual_action)

if __name__ == "__main__":
    if sys.argv[1]=="add_to_csv":
        if os.path.isfile(csv_path)==False:
            print("File doesn't exist. Creating file..")
            column = ["Folder", "Total Files", "Total Fail", "Failure Rate Attempt Kuis", "Failure Rate InfluxDB"]
            add_to_csv(csv_path, column)
            print("File successfuly created!")
        array = [folder_name, total_files, total_failure, failure_rate_attempts, failure_rate_influxdb]
        add_to_csv(csv_path, array)
    else:
        print("Folder Name: {}, Total Files: {}, Total Fail: {}, Failure Rate Attempt Kuis {}, Failure Rate InfluxDB {}".format(folder_name, total_files, total_failure, failure_rate_attempts, failure_rate_influxdb))
        # print("total files: " + str(total_files))
        # print("total fail: " + str(total_fail))
        # print("Failure Rate Attempt Kuis: " + str(failure_rate_attempt))
        # # 1 dikurang ((jumlah data di InfluxDB) dibagi (jumlah bot berhasil * jumlah aktivitas per bot))
        # # misal untuk failure rate 0%: 0 = 1 - (5000/(100*50))
        # print("Failure Rate InfluxDB:", failure_rate_influx)