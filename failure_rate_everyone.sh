#!/bin/sh

for dir in */; do
    cp ./failure_rate_grafana.py $dir
    cd $dir
    python3 failure_rate_grafana.py yay
    cd ..
done

exit 0