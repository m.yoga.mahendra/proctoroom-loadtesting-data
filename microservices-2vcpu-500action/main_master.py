import optparse
import os
import threading

from master_process.MasterPostMan import MasterPostMan
import logging
import socket

logger = logging.getLogger(__name__)

def listen_report_thread(postman, ready_workers, amount_worker_should_be):
    with socket.socket(socket.AF_INET,
                       socket.SOCK_STREAM) as sc:
        sc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        postman.init_listen(sc)
        logging.debug("Postman has initialized listening...")
        while True:
            try:
                connection, address = postman.listen_report()
                ip_address = address[0]
                ready_workers.append(ip_address)
                postman.send_response_200(connection)
            except Exception as e:
                logging.info(e)
                continue
            logging.debug(f"Receive response from IP address: {ip_address}")
            if len(ready_workers) == amount_worker_should_be:
                logging.info("All workers have completed its task...")


def get_ip_addresses(data):
    ip_addresses = []
    for line in data.readlines():
        cline = line.strip().split()
        ip_addr = cline[-1].strip("(").strip(")")
        ip_addresses.append(ip_addr)
    return ip_addresses


def trigger_thread(worker_param, worker_url):
    try:
        MasterPostMan.send_message(worker_param, worker_url)
    except Exception as e:
        logging.warning(e)

def get_parameters(data):
    data = data.readlines()
    headers = data[0].split(",")
    temp_list = []
    for header in headers:
        temp_list.append(header.strip())
    headers = temp_list
    logging.debug(f"headers value: {headers}")
    result = []
    for i in range(1, len(data)):
        datum = data[i].strip()
        logging.debug(f"datum: {datum}")
        datum = datum.split(",")
        temp_dict = {}
        for j in range(len(datum)):
            temp_dict[headers[j]] = datum[j].strip()
        result.append(temp_dict)
    return result


def get_my_ip_address():
    my_ip_addr = os.popen("hostname -I | cut -d' ' -f1")
    return my_ip_addr.read().strip()


def main():
    logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)

    logger.info("The program is running...")

    parser = optparse.OptionParser()
    parser.add_option('-a', '--amount_worker', help='Amount of worker that should be running')
    parser.add_option('-c', '--csv_file', default="inputs-for-workers.csv", help='CSV file to become parameter')
    (options, args) = parser.parse_args()
    amount_worker_should_be = int(options.amount_worker)
    if not amount_worker_should_be:
        parser.error("Amount of worker is not given")
    csv_file_name = options.csv_file
    logger.info(f"User parameter: [csv_file_name: {csv_file_name}, "
                f"amount_worker_should_be: {amount_worker_should_be}]")


    master_ip_address = get_my_ip_address()
    logger.info(f"Master IP Address: {master_ip_address}")

    HOST = master_ip_address
    PORT = 80

    postman = MasterPostMan(HOST, PORT)

    logger.info("Master Postman configuration {HOST: %s, PORT: %d}"
                 % (HOST, PORT))

    with open("selenium_bot_ip_addresses.txt") as data:
        workers_ip_address = get_ip_addresses(data)
        workers_ip_address.remove(master_ip_address)
        logger.debug(f"WORKERS IP ADDRESSES: {workers_ip_address}")

    with open(csv_file_name) as data:
        workers_param = get_parameters(data)
        logger.debug(f"WORKERS PARAM: {workers_param}")

    logger.info("Triggering bots...")
    total_workers = len(workers_ip_address)
    total_workers_param = len(workers_param)
    for i in range(total_workers):
        worker_ip_address = workers_ip_address[i]
        worker_url = f"http://{worker_ip_address}"
        logger.debug(f"worker_url: {worker_url}")
        worker_param = workers_param[i % total_workers_param]
        worker_param = dict(worker_param)

        thread = threading.Thread(target=trigger_thread,
                                  args=(worker_param,
                                        worker_url))
        thread.start()

    ready_workers = []
    threading.Thread(target=listen_report_thread,
                     args=(postman, ready_workers,
                           amount_worker_should_be)).start()
    logger.info("Start listening for report...")

    input_keyboard = None
    while input_keyboard != "exit":
        input_keyboard = input("Give input here: ")
        if input_keyboard == "check":
            print(len(ready_workers))
        if input_keyboard == "whose_failed":
            count = 0
            for ip_address in workers_ip_address:
                if ip_address not in ready_workers:
                    count += 1
                    print(ip_address)
            print(f"Whose failed count: {count}")

    logger.info("Ready to download...")
    for successful_worker in ready_workers:
        with socket.socket(socket.AF_INET,
                           socket.SOCK_STREAM) as sc:
            sc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try:
                filename = postman.download(sc, successful_worker , 80)
            except Exception as e:
                logger.warning(e)
            logger.debug(f"Success downloading: {filename}")
    logger.info("Done downloading all files...")


if __name__ == "__main__":
    main()
